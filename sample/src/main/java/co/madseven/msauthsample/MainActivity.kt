package co.madseven.msauthsample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import co.madseven.msauthapi.MSAuthClient
import co.madseven.msauthapi.MSToken

class MainActivity : AppCompatActivity() {

    companion object {
        const val YOUR_CLIENT_ID: String = "client-id"
        const val YOUR_CLIENT_SECRET: String = "client-secret"
        const val EMAIL: String = "example@icloud.com"
        const val PASSWORD: String = "abcdef"
        const val PHONE_NUMBER: String = "+336xxxxxxxx"
    }

    //Create account or authenticate
    private lateinit var create: Button
    private lateinit var authenticateEmail: Button
    private lateinit var authenticatePhone: Button

    //Get token upon SMS reception
    private lateinit var etOtpCode: EditText
    private lateinit var authorize: Button

    //Handle 2FA
    private lateinit var addPhone: Button
    private lateinit var removePhone: Button

    //send SMS
    private lateinit var etPhone: EditText
    private lateinit var etMessage: EditText
    private lateinit var smsSend: Button


    //local vars
    var voucher: String? = null
    var accessToken: String? = null
    var renewToken: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        create = findViewById(R.id.create)
        authenticateEmail = findViewById(R.id.authenticate_email)
        authenticatePhone = findViewById(R.id.authenticate_phone)
        etOtpCode = findViewById(R.id.et_otp_code)
        authorize = findViewById(R.id.authorize)
        addPhone = findViewById(R.id.add_phone)
        removePhone = findViewById(R.id.remove_phone)
        etPhone = findViewById(R.id.et_phone)
        etMessage = findViewById(R.id.et_message)
        smsSend = findViewById(R.id.sms_send)

        MSAuthClient.initialize(applicationContext, Companion.YOUR_CLIENT_ID,
            Companion.YOUR_CLIENT_SECRET
        )
        configure()
    }

    private fun configure() {

        create.setOnClickListener {
            MSAuthClient.createAccount(
                Companion.EMAIL,
                Companion.PASSWORD,
                Companion.PHONE_NUMBER,
                completion = { message: String?, error: String? ->
                    error?.let {
                        Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
                    } ?: message?.let {
                        Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
                    } ?: run {
                        Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
                    }
                })
        }

        authenticateEmail.setOnClickListener {
            MSAuthClient.authenticate(
                Companion.EMAIL,
                Companion.PASSWORD,
                completion = { mstoken: MSToken?, error: String? ->
                    error?.let {
                        Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
                    } ?: mstoken?.let {
                        if (it.voucher != null) {
                            voucher = it.voucher
                            Toast.makeText(applicationContext, "voucher: "+it.voucher, Toast.LENGTH_SHORT).show()
                        } else if (it.accessToken != null) {
                            accessToken = it.accessToken
                            Toast.makeText(applicationContext, "access_token: "+it.accessToken, Toast.LENGTH_SHORT).show()
                        }
                    } ?: run {
                        Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
                    }
                })
        }

        authenticatePhone.setOnClickListener {
            MSAuthClient.authenticate(
                Companion.PHONE_NUMBER,
                completion = { mstoken: MSToken?, error: String? ->
                    error?.let {
                        Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
                    } ?: mstoken?.let {
                        voucher = it.voucher
                        Toast.makeText(applicationContext,"voucher: " + it.voucher, Toast.LENGTH_SHORT).show()
                    } ?: run {
                        Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
                    }
                })
        }

        authorize.setOnClickListener {
            MSAuthClient.authorize(
                voucher ?: "",
                etOtpCode.text.toString(),
                completion = { mstoken: MSToken?, error: String? ->
                    error?.let {
                        Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
                    } ?: mstoken?.let {
                        accessToken = it.accessToken
                        renewToken = it.renewToken
                        Toast.makeText(applicationContext, "access_token: "+it.accessToken, Toast.LENGTH_SHORT).show()
                    } ?: run {
                        Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
                    }
                })
        }

        addPhone.setOnClickListener {
            MSAuthClient.activateTwoFactors(
                accessToken ?: "",
                Companion.PHONE_NUMBER,
                completion = { message: String?, error: String? ->
                    error?.let {
                        Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
                    } ?: message?.let {
                        Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
                    } ?: run {
                        Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
                    }
                })
        }

        removePhone.setOnClickListener {
            MSAuthClient.deactivateTwoFactors(
                accessToken ?: "",
                completion = { message: String?, error: String? ->
                    error?.let {
                        Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
                    } ?: message?.let {
                        Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
                    } ?: run {
                        Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
                    }
                })
        }

        smsSend.setOnClickListener {
            MSAuthClient.sendMessage(
                etPhone.text.toString(),
                etMessage.text.toString(),
                completion = { message: String?, error: String? ->
                    error?.let {
                        Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
                    } ?: message?.let {
                        Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
                    } ?: run {
                        Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
                    }
                })
        }
    }
}
