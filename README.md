![Android 4.4+](https://img.shields.io/badge/Android-4.4%2B-blue.svg)  ![SDK 19+](https://img.shields.io/badge/SDK-19%2B-orange.svg)


# MSAuthentication-Android


### Presentation

**MSAuthentication** is a library to perform a strong authentication with 2FA option or to 
perform a phone number validation (weak authentication with SMS).


## Installation

### Repository

Add a new maven repository `url` to your repositories in the global build.gradle
```rb
allprojects {
    repositories {
        google()
        jcenter()
        maven {
            url 'https://gitlab.com/api/v4/projects/19922794/packages/maven'
        }
    }
}
```

And add the following entry to your module build.gradle :

```rb
implementation "co.madseven.msauthapi:msauthentication:1.0.0"
```


## Usage

Check the `co.madseven.msauthsample.MainActivity` for usage.

You need first to initialize the SDK with your credentials (client id and client secret), then you can 
 - create an account (with or without 2FA) in the case of email authentication (strong auth)
 - authenticate with login/email or with only a phone number (for phone verification/weak auth purpose)
 - authorize a login/email couple or a phone number upon receiving a SMS code
 - renew an expired token if necessary
 - enable/disable the 2FA when authenticated
 - send custom SMSes.



### Instantiating the client

```kotlin
val client = MSAuthClient.initialize(context, "my-client-id", "my-client-secret")
```


### List of available endpoints

```kotlin
fun createAccount(email: String, password: String, phoneNumber: String?, completion: (message: String?, error: String?) -> Void)

fun authenticate(email: String, password: String, completion: (mstoken: MSToken?, error: String?) -> Void)

fun authenticate(phoneNumber: String, completion: (mstoken: MSToken?, error: String?) -> Void)

fun authorize(voucher: String, otp: String, completion: (mstoken: MSToken?, error: String?) -> Void)

fun renewToken(refresh_token: String, completion: (mstoken: MSToken?, error: String?) -> Void)

fun activateTwoFactors(access_token: String, phoneNumber: String, completion: (message: String?, error: String?) -> Void)

fun deactivateTwoFactors(access_token: String, phoneNumber: String, completion: (message: String?, error: String?) -> Void)

fun sendMessage(phoneNumber: String, message: String, completion: (message: String?, error: String?) -> Void)
```


## Authentication

### Authenticate with a email/password 


1. First you need to create an account :

```kotlin
MSAuthClient.createAccount("my-email-address", "my-password", "2FA-phone-number", completion = { message: String?, error: String? ->
        error?.let {
            Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
        } ?: message?.let {
            Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
        } ?: run {
            Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
        }
    })
```

It sends a email with a confirmation link. The link provided in the email (sent to the mail box) redirect to an url scheme
of type `msauthxxxxxxxxx://signup?email=example@icloud.com`  where the `xxxxxxxxx`  is your `clientId`


2. Then you can authenticate :

Authenticate with the email and password (if the account was created with a 2FA number then it send an SMS with a code automatically, otherwise not).

```kotlin
MSAuthClient.authenticate(
    Companion.EMAIL,
    Companion.PASSWORD,
    completion = { mstoken: MSToken?, error: String? ->
        error?.let {
            Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
        } ?: mstoken?.let {
            if (it.voucher != null) {
                voucher = it.voucher
                Toast.makeText(applicationContext, "voucher: "+it.voucher, Toast.LENGTH_SHORT).show()
            } else if (it.accessToken != null) {
                accessToken = it.accessToken
                Toast.makeText(applicationContext, "access_token: "+it.accessToken, Toast.LENGTH_SHORT).show()
            }
        } ?: run {
            Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
        }
    })
```
It gives back either a `voucher` (in case of 2FA or phone verification) or directly the final `access_token` in case of regular authentication (without 2FA)


3. Verify the code and get the access_token

In case of 2FA authentication or phone verification, use the `voucher` you got from the authentication step and the `code` you received in the SMS and call the authorize() method :

```kotlin
MSAuthClient.authorize(
    voucher ?: "",
    etOtpCode.text.toString(),
    completion = { mstoken: MSToken?, error: String? ->
        error?.let {
            Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
        } ?: mstoken?.let {
            accessToken = it.accessToken
            renewToken = it.renewToken
            Toast.makeText(applicationContext, "access_token: "+it.accessToken, Toast.LENGTH_SHORT).show()
        } ?: run {
            Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
        }
    })
```

You get your final access_token and a renew_token to renew it once it is expired.



### Authenticate with a phone number or perform a phone number verification

You need first to authenticate with a phone number, it will sends automatically a SMS with a `code` to the provided phone number :

```kotlin
MSAuthClient.authenticate(
    Companion.PHONE_NUMBER,
    completion = { mstoken: MSToken?, error: String? ->
        error?.let {
            Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
        } ?: mstoken?.let {
            voucher = it.voucher
            Toast.makeText(applicationContext,"voucher: " + it.voucher, Toast.LENGTH_SHORT).show()
        } ?: run {
            Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
        }
    })
```

It gives a `voucher` along with the `code` received by SMS.
Use this `voucher` and the `code` and call the authorize() method :

```kotlin
MSAuthClient.authorize(
    voucher ?: "",
    etOtpCode.text.toString(),
    completion = { mstoken: MSToken?, error: String? ->
        error?.let {
            Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
        } ?: mstoken?.let {
            accessToken = it.accessToken
            renewToken = it.renewToken
            Toast.makeText(applicationContext, "access_token: "+it.accessToken, Toast.LENGTH_SHORT).show()
        } ?: run {
            Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
        }
    })
```

You get your access_token and a renew_token to renew it once it is expired.
It means aswell the phone number is correct since the user received his code.



## Send a SMS

### Send a SMS with a custom content

You need to call sendMessage() with a valid phone number and a not empty message :

```kotlin
MSAuthClient.sendMessage(
    etPhone.text.toString(),
    etMessage.text.toString(),
    completion = { message: String?, error: String? ->
        error?.let {
            Toast.makeText(applicationContext, "Error : $error", Toast.LENGTH_SHORT).show()
        } ?: message?.let {
            Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
        } ?: run {
            Toast.makeText(applicationContext, "Empty response", Toast.LENGTH_SHORT).show()
        }
    })
```


